class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
      result = str.gsub(/\w+/) do |word|
          if word.length > 4 then
              "#{word[/^[A-Z]/] ? 'M' : 'm' }arklar"
          else
              word
          end
       end

      return result  
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    fib = Hash.new{ |h, n| n < 2 ? h[n] = n : h[n] = h[n-1] + h[n-2] }
    result = Array.new(nth){ |n| fib[n] }
    result = result.keep_if(&:even?).reduce(:+)

    return result
  end

end
